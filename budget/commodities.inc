commodity USD
   note US dollars
   format 1,000.00
   alias $
   nomarket

commodity ZAR
   note South African Rand
   format 1,000.00
   alias R
   nomarket
   default

commodity EUR
   note Euros
   format 1,000.00
   nomarket
   default

